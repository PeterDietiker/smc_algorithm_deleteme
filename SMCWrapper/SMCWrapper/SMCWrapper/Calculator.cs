﻿using System;
using SMCVisualization.Calculate;

using AlgorithmFrameworkCore.InterimResults;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using RGiesecke.DllExport;

namespace Calculator
{
    public class Calculator
    {
        [DllExport("addPhotonData", CallingConvention = CallingConvention.Cdecl)]
        unsafe public static int addPhotonData( double[] photondata, int N, ref int deprime,ref int threshold)
        {
            // save read data in XML format
            List<uint> listB = new List<uint>();
            for (int i = 0; i < N; i++)
            {
                listB.Add((uint)photondata[i]);
            }
           
            Report rpt = Report.ReadFromFile(@"C:\Daten\Python\RDP_simulator\SMC_algorithm\ActualValues.xml");
          
            rpt.RawData = new RawData(listB);
            
            IntermediateResults ir = new IntermediateResults() ;
            ir = ExecuteAlgorithm.pyCalcSample_NoMasterCurve(rpt, -1, 0, false);
         
            deprime = (int)ir.AS41.DEPrime_NoTemp;
            double NSigma = (Double)rpt.DataEvaluationInput.EvaluationVariables.NSigma;
            threshold = (int)(ir.AS81.AverageEstimatedNoise + NSigma * Math.Sqrt(ir.AS81.AverageEstimatedNoise));


            return 0;
           
        }


    }
}